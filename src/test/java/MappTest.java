import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import page.MappTestPage;

/**
 * Created by kuba on 6/11/17.
 */
public class MappTest extends TestBase {

    @Test
    public void checkSeleniumPlayground() {

        MappTestPage mappTestPage = new MappTestPage(getDriver());

        //1
        mappTestPage.enterTitleOfPage(getDriver().getTitle());

        //2
        mappTestPage.enterName("Kilgore Trout");

        //3
        mappTestPage.setOccupationSciFiAuthor();

        //4
        mappTestPage.enterNumberOfBlueBoxes(Integer.toString(getDriver().findElements(By.cssSelector(".bluebox")).size()));

        //5
        mappTestPage.clickClickMeLink();

        //6
        mappTestPage.enterClassOfRedBox(getDriver().findElement(By.cssSelector("#redbox")).getAttribute("class"));

        //7
        ((JavascriptExecutor)getDriver()).executeScript("ran_this_js_function();");

        //8
        mappTestPage.enterJsReturnValue((String) ((JavascriptExecutor) getDriver())
                .executeScript("return got_return_from_js_function();").toString());

        //9
        mappTestPage.checkWroteBook();

        //10
        mappTestPage.enterTextFromRedBox(getDriver().findElement(By.cssSelector("#redbox")).getText());

        //11
        if (getDriver()
                .findElement(By.xpath("(//h3/following::script/following::span)[last()-1]")).getText().contains("orange"))
            mappTestPage.enterColorBoxOnTop("orange");
        else mappTestPage.enterColorBoxOnTop("green");

        //12
        getDriver().manage().window().setSize(new Dimension(850, 650));

        //13
        if (getDriver().findElements(By.xpath("//*[contains(@id, 'ishere')]")).size() == 0)
            mappTestPage.enterAnswerForIshere("no");
        else mappTestPage.enterAnswerForIshere("yes");

        //14
        if (!getDriver().findElement(By.xpath("//*[contains(@id, 'purplebox')]")).isDisplayed())
            mappTestPage.enterAnswerForPurpleBox("no");
        else mappTestPage.enterAnswerForPurpleBox("yes");

        //15
        mappTestPage.clickClickThenWait();
        Waits.waitForElementToBeAvailableAndClickable(getDriver(), By.xpath("//a[contains(@onclick, 'click_after_wait')]"));
        mappTestPage.clickClickAfterWait();

        //16
        getDriver().switchTo().alert().accept();
        getDriver().switchTo().defaultContent();

        //17
        mappTestPage.clickSubmitButton();
    }
}
