import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by Jakub Oczkowski-Luczkos on 12.06.17.
 */
public class Waits {

    public static void waitForElementToBeAvailableAndClickable(WebDriver driver, By by) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
       .withTimeout(10, SECONDS)
       .pollingEvery(100, MILLISECONDS)
       .ignoring(NoSuchElementException.class);

        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(by)));
    }
}
