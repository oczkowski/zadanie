import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by kuba on 6/11/17.
 */
public class WebDriverInstance {

    private static WebDriver driver;

    public static WebDriver instantiate() {
        if (!System.getProperty("os.name").contains("Linux"))
            System.setProperty("webdriver.chrome.driver", "chromedriver/win32/chromedriver.exe");
        return driver = new ChromeDriver();
    }

    public static WebDriver getDriver() {
        if (driver == null)
            return instantiate();
        else return driver;
    }
}
