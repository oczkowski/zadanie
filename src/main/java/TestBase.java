
import org.openqa.selenium.WebDriver;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by kuba on 6/11/17.
 */
public class TestBase {
    private WebDriver driver;
    protected static String MAPP_TEST_URL = System.getProperty("mapp.test.url");

    protected TestBase() {
        driver = WebDriverInstance.instantiate();

        driver.manage().window().maximize();
    }

    @BeforeTest(alwaysRun = true)
    protected void init() {
        driver.get(MAPP_TEST_URL);
    }

    @AfterTest(alwaysRun = true)
    protected void halt() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

}
