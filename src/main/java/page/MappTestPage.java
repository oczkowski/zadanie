package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.security.PublicKey;

/**
 * Created by kuba on 6/11/17.
 */
public class MappTestPage {

    private WebDriver driver;

    public MappTestPage(WebDriver driver) {
        this.driver = driver;
    }
    public MappTestPage enterTitleOfPage(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer1"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage enterName(String value) {
        WebElement input = driver.findElement(By.cssSelector("#name"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage setOccupationSciFiAuthor() {
        new Select(driver.findElement(By.cssSelector("#occupation"))).selectByValue("scifiauthor");
        return this;
    }

    public MappTestPage enterNumberOfBlueBoxes(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer4"));
        input.click();
        input.sendKeys(value);
//        driver.findElement(By.cssSelector("#answer4")).sendKeys(value);
        return this;
    }

    public MappTestPage clickClickMeLink() {
        driver.findElement(By.xpath("//a[text()='click me']")).click();
        return this;
    }

    public MappTestPage enterClassOfRedBox(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer6"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage enterJsReturnValue(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer8"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage checkWroteBook() {
        driver.findElement(By.cssSelector("input[value='wrotebook']")).click();
        return this;
    }

    public MappTestPage enterTextFromRedBox(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer10"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage enterColorBoxOnTop(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer11"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage enterAnswerForIshere(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer13"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage enterAnswerForPurpleBox(String value) {
        WebElement input = driver.findElement(By.cssSelector("#answer14"));
        input.click();
        input.sendKeys(value);
        return this;
    }

    public MappTestPage clickClickThenWait() {
        driver.findElement(By.xpath("//p/a")).click();
        return this;
    }

    public MappTestPage clickClickAfterWait() {
        driver.findElement(By.xpath("//a[contains(@onclick, 'click_after_wait')]")).click();
        return this;
    }

    public MappTestPage clickSubmitButton() {
        driver.findElement(By.cssSelector("input#submitbutton")).click();
        return this;
    }
}
